﻿using Microsoft.AspNetCore.Hosting;
using NLog;
using NLog.Web;
using System;
using Microsoft.Extensions.Logging;
using Coscine.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Coscine.Logging;
using System.Collections.Generic;

namespace Coscine.ApiCommons
{
    public abstract class AbstractProgram<T> where T : IConfiguration, new()
    {
        public static IConfiguration Configuration = new T();

        public static void InitializeWebService<ST>() where ST : AbstractDefaultStartup, new()
        {
            var configurator = new Configurator(Configuration);
            configurator.Register();
            StartWebService<ST>(configurator);
        }

        public static void InitializeInternalWebService<ST>() where ST : AbstractDefaultStartup, new()
        {
            var configurator = new Configurator(new InternalApplicationInformation(), Configuration);
            configurator.Register();
            StartWebService<ST>(configurator);
        }

        private static void StartWebService<ST>(Configurator configurator) where ST : AbstractDefaultStartup, new()
        {
            ST startup = new ST();
            startup.SetBasePath(configurator.ApplicationInformation);

            // Load NLog Configuration
            CoscineLoggerConfiguration.SetConfig();

            var logger = LogManager.GetCurrentClassLogger();
            try
            {
                logger.Debug("Initialize Main Method");

                var host = new WebHostBuilder()
                .ConfigureServices(services =>
                {
                    services.AddSingleton(startup);
                })
                .UseStartup<ST>()
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseNLog()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseUrls($"http://[::]:{configurator.ApplicationInformation.Port}")
                .Build();

                host.Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }
    }
}

