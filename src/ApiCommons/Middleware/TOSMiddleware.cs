﻿using Coscine.Database.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Coscine.ApiCommons.Middleware
{
    public class TOSMiddleware
    {
        private readonly RequestDelegate _next;

        public TOSMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var userId = context.User?.FindFirst("UserID")?.Value;
            if (userId != null && Guid.TryParse(userId, out Guid userIdGuid) && !context.Request.Path.Value.Contains("TOS"))
            {
                TOSModel tosModel = new TOSModel();
                var tosAcceptedList = tosModel.GetAllWhere((entry) => entry.UserId == userIdGuid);
                var currentTos = Configurator.Configuration.GetStringAndWait("coscine/global/tos/version");
                var tosAccepted = tosAcceptedList != null 
                    && tosAcceptedList.Any((entry) => entry.Version == currentTos);
                if (!tosAccepted)
                {
                    var result = JsonConvert.SerializeObject(
                        new { error = $"The TOS of version {currentTos} have not been accepted!" });
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    await context.Response.WriteAsync(result);
                    return;
                }
            }

            await _next(context);
        }
    }
}
