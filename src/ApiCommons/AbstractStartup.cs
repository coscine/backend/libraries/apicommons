﻿using Coscine.ApiCommons.Middleware;
using Coscine.JwtHandler;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace Coscine.ApiCommons
{
    /// <summary>
    /// This Startup applies the JWT token authentification additionally.
    /// </summary>
    public abstract class AbstractStartup : AbstractDefaultStartup
    {
        private JWTHandler _jWTHandler;
        private ApplicationInformation _applicationInformation;

        public override void SetBasePath(ApplicationInformation applicationInformation)
        {
            base.SetBasePath(applicationInformation);
            _jWTHandler = new JWTHandler(GetConfiguration());
            _applicationInformation = applicationInformation;
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            ConfigureServicesExtension(services);

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddControllersWithViews().AddNewtonsoftJson();

            var key = _jWTHandler.GetSecurityKey();
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidAudience = "https://coscine.rwth-aachen.de",
                    ValidIssuer = "https://coscine.rwth-aachen.de",
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = key,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            ConfigureServicesExtensionLate(services);
        }

        public override void ConfigureServicesExtensionLate(IServiceCollection services)
        {
            base.ConfigureServicesExtensionLate(services);

            // Register the Swagger services
            services.AddOpenApiDocument((settings) =>
            {
                settings.Title = _applicationInformation.AppName;
                settings.Version = _applicationInformation.Version.ToString();
                settings.PostProcess = (postProcess) =>
                {
                    postProcess.Host = _applicationInformation.ApiUrl;
                };
                settings.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT token"));
                settings.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT token", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    Description = "Copy 'Bearer ' + valid JWT token into field",
                    In = OpenApiSecurityApiKeyLocation.Header
                }));
            });
        }

        public override void ConfigureExtensionMiddleware(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureExtensionMiddleware(app, env);

            app.UseMiddleware<TOSMiddleware>();
        }

        public override void ConfigureExtensionLate(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureExtensionLate(app, env);

            app.UseStaticFiles();
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }

    }
}