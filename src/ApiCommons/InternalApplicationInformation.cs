﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Coscine.ApiCommons
{
    public class InternalApplicationInformation: ApplicationInformation
    {
        public override Dictionary<string, string> TraefikValues
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { $"{TraefikBackendPath}/url", $"localhost:{Port}"},
                    { $"{TraefikBackendPath}/weight", $"{1}"},
                    { $"{TraefikFrontendPath}/backend", AppName},
                    { $"{TraefikFrontendPath}/routes/{AppName}/rule", $"Host:localhost;PathPrefix:/{PathPrefix}"}
                };
            }
        }
    }
}
