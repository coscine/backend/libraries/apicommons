﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.JwtHandler;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Coscine.ApiCommons
{
    public class Authenticator
    {
        private readonly Controller _controller;
        private readonly IConfiguration _configuration;
        private static readonly ApiTokenModel _apiTokenModel = new ApiTokenModel();

        public Authenticator(Controller controller, IConfiguration configuration)
        {
            _controller = controller;
            _configuration = configuration;
        }

        [Obsolete("This method will soon be deprecated. Use GetUser instead.")]
        public bool ValidUser()
        {
            try
            {
                GetUserFromToken();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static string GetUserId(IEnumerable<Claim> claims)
        {
            try
            {
                var userId = claims.Where(x => x.Type.ToLower() == "userId".ToLower()).FirstOrDefault()?.Value;
                var tokenId = claims.Where(x => x.Type.ToLower() == "tokenId".ToLower()).FirstOrDefault()?.Value;

                // Only one can be set
                if (userId != null && tokenId != null)
                {
                    return null;
                }

                if (userId != null)
                {
                    return userId;
                }

                if (tokenId != null)
                {
                    var token = _apiTokenModel.GetById(new Guid(tokenId));
                    return token.UserId.ToString();
                }

                return null;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        public string GetUserId()
        {
            return GetUserId(_controller.HttpContext.User.Claims);
        }

        public User GetUser()
        {
            return GetUser(GetUserId());
        }

        public User GetUser(string userId)
        {
            if(string.IsNullOrWhiteSpace(userId))
            {
                return null;
            }

            var userModel = new UserModel();
            try
            {
                return userModel.GetById(Guid.Parse(userId));
            }
            catch (Exception)
            {
                return null;
            }
        }

        [Obsolete("This method will soon be deprecated. Use GetUser instead.")]
        public User GetUserFromToken()
        {
            var authorization = _controller.Request.Headers["Authorization"].ToArray();
            string bearer = null;
            foreach (var line in authorization)
            {
                if (line.Contains("Bearer"))
                {
                    bearer = line;
                }
            }
            if (!string.IsNullOrWhiteSpace(bearer))
            {
                bearer = bearer.Replace("Bearer", "").Trim();
                JWTHandler jwtHandler = new JWTHandler(_configuration);
                var claims = jwtHandler.GetContents(bearer);
                var userClaim = (from claimObj in claims
                                 where claimObj.Type == "UserId"
                                 select claimObj).First();
                var userModel = new UserModel();
                return userModel.GetById(Guid.Parse(userClaim.Value));
            }
            else
            {
                throw new ArgumentException("Bearer Token is not set!");
            }
        }

        [Obsolete("This method will soon be deprecated. Use GetUser instead.")]
        public void ValidateAndExecute(Action<User> action)
        {
            action.Invoke(GetUserFromToken());
        }

        [Obsolete("This method will soon be deprecated. Use GetUser instead.")]
        public T ValidateAndExecute<T>(Func<User, T> func)
        {
            return func.Invoke(GetUserFromToken());
        }
    }
}
