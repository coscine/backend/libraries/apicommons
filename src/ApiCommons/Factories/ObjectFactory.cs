﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.ApiCommons.Factories
{
    public class ObjectFactory<T>
    {
        public static Stream SerializeToStream(T o)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            var json = JsonConvert.SerializeObject(o);
            writer.Write(json);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static T DeserializeFromStream(Stream body)
        {
            var sr = new StreamReader(body);
            var json = sr.ReadToEnd();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
