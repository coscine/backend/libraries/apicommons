﻿using Coscine.ApiCommons.Middleware;
using Coscine.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Coscine.ApiCommons
{
    public abstract class AbstractDefaultStartup
    {
        private string _basePath;
        private IConfiguration _configuration { get; set; }

        internal IConfiguration GetConfiguration()
        {
            return _configuration;
        }

        public virtual void SetBasePath(ApplicationInformation applicationInformation)
        {
            _basePath = $"/{applicationInformation.PathPrefix}";
            _configuration = new ConsulConfiguration();
        }

        public virtual void ConfigureServicesExtension(IServiceCollection services)
        {

        }

        public virtual void ConfigureServicesExtensionLate(IServiceCollection services)
        {

        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            ConfigureServicesExtension(services);

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddControllersWithViews().AddNewtonsoftJson();

            ConfigureServicesExtensionLate(services);
        }

        public virtual void ConfigureExtension(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }

        // Add Middlewares which need the User to be existent
        public virtual void ConfigureExtensionMiddleware(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }

        public virtual void ConfigureExtensionLate(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureExtension(app, env);

            app.UseDeveloperExceptionPage();

            app.UseMiddleware<LoggingMiddleware>();
            app.UseCors(builder => builder
            .SetIsOriginAllowed(_ => true)
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());

            app.UseAuthentication();

            ConfigureExtensionMiddleware(app, env);

            app.UsePathBase(_basePath);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "text/plain";

                await context.HttpContext.Response.WriteAsync(
                    "Status code page, status code: " +
                    context.HttpContext.Response.StatusCode);
            });

            ConfigureExtensionLate(app, env);
        }
    }
}
